package ba.etf.rma21.projekat.data.models

import retrofit2.Response
import retrofit2.http.*

interface Api {

    @GET("/predmet")//vraca sve predmete
    suspend fun getPredmeti(): List<Predmet>

    @GET("/predmet/{id}")//vraca predmet sa zadanim id-em
    suspend fun getPredmetId(@Path("id") id:Int): Predmet

    @GET("/predmet/{id}/grupa")//vraca listu grupa na predmetu sa zadanim id-em
    suspend fun getGrupeZaPredmet(@Path("id") id:Int): List<Grupa>

    @GET("/grupa")//vraca sve grupe
    suspend fun getGrupe(): List<Grupa>

    @GET("/kviz")//vraca sve kvizove
    suspend fun getAll(): List<Kviz>

    @GET("/kviz/{id}")//vraca kviz sa id-em ili poruku da kviz ne postoji
    suspend fun getById(@Path("id") id:Int): Kviz

    @GET("/grupa/{id}/kvizovi")//vraca kvizove koji su dodijeljeni ovoj grupi
    suspend fun getKvizoviZaGrupu(@Path("id") id:Int): Array<Kviz>

    @GET("/student/{id}")//vraca account studenta koji ima zadati hash
    suspend fun getStudenta(@Path("id") hash:String): Account?

    @GET("/kviz/{id}/pitanja")//vraca pitanja za kviz
    suspend fun getPitanja(@Path("id") id:Int): List<Pitanje>

    @GET("/student/{id}/grupa")//vraca grupe u koje je student upisan
    suspend fun getUpisane(@Path("id") hash:String): List<Grupa>

    @GET("/student/{id}/kviztaken/{ktid}/odgovori")//vraca listu odgovora za pokusaja rjesavanja kviza sa id-em ktid i za
    //studenta sa zadanim hash id-em
    suspend fun getOdgovoriKviz(@Path("id") id:String,@Path("ktid") ktid:Int): List<Odgovor>

    @POST("/grupa/{gid}/student/{id}")
    suspend fun upisiUGrupu(@Path("gid") idGrupe: Int, @Path("id") hashStudent: String) : Message

    @POST("/student/{id}/kviztaken/{kid}/odgovor")
    suspend fun postaviOdgovorKviz(@Path("id") hash: String, @Path("kid")idKvizTaken: Int, @Body odgovor: OdgovorBody)

    @GET("/student/{id}/kviztaken")//vraca listu pokusaja za studenta sa zadanim hash id-em
    suspend fun getPocetiKvizovi(@Path("id") hash:String): List<KvizTaken>

    @POST("/student/{id}/kviz/{kid}")//zapocinje odgovaranje studenta sa id-em id na kvizu kid
    suspend fun zapocniKviz(@Path("id") hash:String, @Path("kid") idKviza:Int): KvizTaken

    @DELETE("/student/{id}/upisugrupeipokusaji")//brise sve podatke povezane sa korisnikom koji ima hash koji je
    //proslijedjen u url-u, greska da korisnik ne postoji
    suspend fun obrisiPodatkeZaStudenta(@Path("id") hash:String): Message

    @GET("/grupa/{id}")//vraca grupu sa id-em grupa
    suspend fun getGrupaId(@Path("id") id:Int): Grupa

    @GET("/kviz/{id}/grupa")//vraca grupe u kojima je kviz dostupan
    suspend fun getGrupeKvizDostupan(@Path("id") id:Int): List<Grupa>













}