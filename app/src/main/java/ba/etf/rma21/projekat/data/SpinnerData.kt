package ba.etf.rma21.projekat.data

class SpinnerData {

    companion object {
        private var spinner1 = 0
        private var spinner2 = 0
        private var spinner3 = 0

        fun setSpinner1(indeks: Int) {
            spinner1 = indeks
        }

        fun getSpinner1(): Int {
            return spinner1
        }

        fun setSpinner2(indeks: Int) {
            spinner2 = indeks
        }

        fun getSpinner2(): Int {
            return spinner2
        }

        fun setSpinner3(indeks: Int) {
            spinner3 = indeks
        }

        fun getSpinner3(): Int {
            return spinner3
        }
    }
}