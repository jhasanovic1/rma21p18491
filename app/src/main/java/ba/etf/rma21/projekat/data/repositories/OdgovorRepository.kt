package ba.etf.rma21.projekat.data.repositories

import ba.etf.rma21.projekat.data.models.*
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import java.util.*

class OdgovorRepository {
    //getOdgovoriKviz(idKviza:Int):List<Odgovor> - vraća listu odgovora za kviz, praznu listu ako
    // student nije odgovarao ili nije upisan na zadani kviz
    //postaviOdgovorKviz(idKvizTaken:Int,idPitanje:Int,odgovor:Int):Int - funkcija postavlja odgovor
    // sa indeksom odgovor na pitanje sa id-em idPitanje u okviru pokušaja sa id-em idKvizTaken.
    // Funkcija vraća ukupne bodove na kvizu nakon odgovora ili -1 ukoliko ima neka greška u zahtjevu

    companion object {

        suspend fun getOdgovoriKviz(idKviza: Int): List<Odgovor> {
            //ako student nije odgovarao ili nije upisan na kviz vratiti praznu listu
            return withContext(Dispatchers.IO) {
                val pocetiKvizovi = TakeKvizRepository.getPocetiKvizovi()
                var odgovarao = false
                var id = 0
                if (pocetiKvizovi != null) {
                    for (kviz in pocetiKvizovi) {
                        if (kviz.KvizId == idKviza) {
                            odgovarao = true
                            id = kviz.id//PROVJERITI JE LI SE U GET SALJE ID POKUSAJA ILI ID KVIZA
                            break
                        }
                    }
                }
                val upisaniKvizovi = KvizRepository.getUpisani()
                var upisan = false
                for (k in upisaniKvizovi) {
                    if (k.id == idKviza) {
                        upisan = true
                        break
                    }
                }
                val lista: List<Odgovor> = emptyList()
                if (odgovarao == false || upisan == false) return@withContext lista
                return@withContext ApiAdapter.retrofit.getOdgovoriKviz(
                    AccountRepository.getHash(),
                    id
                )
            }
        }

        suspend fun postaviOdgovorKviz(idKvizTaken: Int, idPitanje: Int, odgovor: Int): Int {
            return withContext(Dispatchers.IO) {
                val zapocetiPokusaji = TakeKvizRepository.getPocetiKvizovi()
                var kvizId = 0
                if (zapocetiPokusaji != null) {
                    for (kviz in zapocetiPokusaji) {
                        if (kviz.id == idKvizTaken) kvizId = kviz.KvizId
                    }
                }
                //greska
                /*val m = postaviOdgovor(idKvizTaken,idPitanje,odgovor)
            if(m.message == "KvizTaken not found." || m.message.contains("Ne postoji account gdje je hash=")
                || m.message == "Pitanje not found." || m.message=="Greška pri dodavanju odgovora, vjerovatno je već odgovoreno na ovo pitanje!")
                return@withContext -1*/
                var tacnih = 0
                val pitanjaZaKviz = PitanjeKvizRepository.getPitanja(kvizId)
                if (kvizId != 0) {
                    val dosadasnjiOdgovori = getOdgovoriKviz(idKvizTaken)
                    for (p in pitanjaZaKviz) {
                        if (p.id == idPitanje && p.tacan == odgovor) tacnih=tacnih+1
                        for (odg in dosadasnjiOdgovori)
                            if (odg.pitanjeId == p.id && odg.odgovoreno == p.tacan){ tacnih=tacnih+1
                                break
                            }

                    }
                    val procenat = (tacnih*100) / pitanjaZaKviz.size
                    ApiAdapter.retrofit.postaviOdgovorKviz(
                        AccountRepository.acHash, idKvizTaken,
                        OdgovorBody(odgovor, idPitanje, procenat.toInt())
                    )
                    return@withContext procenat.toInt()
                }

                return@withContext -1
            }
        }
    }
}


