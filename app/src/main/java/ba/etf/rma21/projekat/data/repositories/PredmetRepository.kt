package ba.etf.rma21.projekat.data.repositories

import ba.etf.rma21.projekat.data.models.Predmet


class PredmetRepository {
    companion object {
       //var upisaneGrupeiPredmeti = mutableMapOf<Predmet, Grupa>()

        init {
            /*for(ug in pocetneUpisaneGrupe()){
                //za svaku grupu pronaci njen predmet i spojiti
                for(up in pocetniUpisaniPredmeti()){
                    if(ug.nazivPredmeta.equals(up.naziv))
                        upisaneGrupeiPredmeti.put(up,ug)
                }
            }*/
        }

        /*fun upisiPredmet(nazivPredmeta: String,nazivGrupe: String): Unit {
            lateinit var predmet:Predmet
            lateinit var grupa:Grupa
            for(p in predmeti()){
                if(p.naziv.equals(nazivPredmeta)) predmet=p
            }
            for(g in grupe()){
                if(g.naziv.equals(nazivGrupe)) grupa=g
            }
            upisaneGrupeiPredmeti.put(predmet,grupa)
        }*/

        suspend fun getPredmetsByGodina(godina: Int):List<Predmet>{
            var predmetiNaGodini:ArrayList<Predmet> = arrayListOf()
            for(predmet in PredmetIGrupaRepository.getPredmeti()){
                if(predmet.godina==godina)
                    predmetiNaGodini.add(predmet)
            }
            return predmetiNaGodini
        }

        /*fun getAll() : List<Predmet>{
            return predmeti()
        }

        fun getUpisani() : List<Predmet>{
            var upisani: ArrayList<Predmet> = arrayListOf()
            for(m in upisaneGrupeiPredmeti)
                upisani.add(m.key)
            return upisani
        }*/

        /*fun getNaziviNeupisanihPredmetaNaGodini(godinaStudija:Int):ArrayList<String>{
            val neupisani:ArrayList<String> = arrayListOf()
            val upisani:ArrayList<Predmet> = arrayListOf()
            for(m in upisaneGrupeiPredmeti)
                upisani.add(m.key)

            for(predmet in predmeti()){
                if(predmet.godina==godinaStudija && !upisani.contains(predmet))
                    neupisani.add(predmet.naziv)
            }
            return neupisani
        }*/

    }

}
