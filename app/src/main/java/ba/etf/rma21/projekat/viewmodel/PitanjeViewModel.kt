package ba.etf.rma21.projekat.viewmodel

import ba.etf.rma21.projekat.data.models.Kviz
import ba.etf.rma21.projekat.data.models.Pitanje
import ba.etf.rma21.projekat.data.models.Predmet
import ba.etf.rma21.projekat.data.repositories.KvizRepository
import ba.etf.rma21.projekat.data.repositories.PitanjeRepository
import ba.etf.rma21.projekat.data.repositories.PredmetIGrupaRepository
import ba.etf.rma21.projekat.data.repositories.PredmetRepository
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.Job
import kotlinx.coroutines.launch

class PitanjeViewModel {
    val scope = CoroutineScope(Job() + Dispatchers.Main)

    fun getPonudjeniOdgovori(pitanje:Pitanje, onSuccess: (odgovori: List<String>) -> Unit,
                    onError: () -> Unit) {
        scope.launch {
            val result = PitanjeRepository.getOdgovore(pitanje)
            when (result) {
                is List<String> -> onSuccess.invoke(result)
                else -> onError.invoke()
            }
        }
    }


    /*fun getPonudjeniOdgovori(pitanje: Pitanje):List<String>{
        return PitanjeRepository.getOdgovore(pitanje)
    }*/
}