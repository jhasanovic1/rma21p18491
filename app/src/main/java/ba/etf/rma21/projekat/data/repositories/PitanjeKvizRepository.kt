package ba.etf.rma21.projekat.data.repositories

import ba.etf.rma21.projekat.data.models.ApiAdapter
import ba.etf.rma21.projekat.data.models.Kviz
import ba.etf.rma21.projekat.data.models.Pitanje
import ba.etf.rma21.projekat.data.models.PitanjeKviz

import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext

class PitanjeKvizRepository {


    companion object {

//        fun getPitanja(idKviza:Int):List<Pitanje> {- vraća sva pitanja na kvizu sa zadanim id-em
//        return pitanjaKviz.filter{it.id==idKviza}
//        }

        /*fun getPitanja(nazivKviza:String,nazivPredmeta:String):List<Pitanje>{
            var listaPitanja: MutableList<Pitanje> = mutableListOf()
            for(pitanje in pitanja()) {
                for (pitanjeKviz in pitanjakviz()) {
                    if (pitanjeKviz.naziv.equals(pitanje.naziv) && pitanjeKviz.kviz.equals(
                            nazivKviza) && pitanjeKviz.predmet.equals(nazivPredmeta))
                        listaPitanja.add(pitanje)
                }
            }
            return listaPitanja
        }
*/
        /*fun getOdgovor(kviz:Kviz,pitanjeId:String):Int{
            var odgovor:Int=-1
            for(p in pitanjaKviz){
                if(p.naziv.equals(pitanjeId) && p.predmet.equals(kviz.nazivPredmeta) && p.kviz.equals(kviz.naziv))
                    odgovor=p.odgovor
            }
            return odgovor
        }

        fun dodajOdgovor(kviz: Kviz, pitanjeId:String, odgovor:Int){
            for(p in pitanjaKviz){
                if(p.naziv.equals(pitanjeId) && p.kviz.equals(kviz.naziv) && p.predmet.equals(kviz.nazivPredmeta)) p.odgovor=odgovor
            }
        }*/

        /*fun getProcenat(kviz:String,predmet:String):Float{
            var tacni=0
            for(k in KvizRepository.sviKvizovi){
                if(k.naziv.equals(kviz) && k.nazivPredmeta.equals(predmet)){
                    tacni=k.tacnih
                }
            }
            var procenat:Float= 0F
            procenat=tacni.toFloat()/getPitanja(kviz,predmet).size
            return procenat
        }*/

            suspend fun getPitanja(idKviza: Int): List<Pitanje> {
                return withContext(Dispatchers.IO) {
                    return@withContext ApiAdapter.retrofit.getPitanja(idKviza)
                }
            }

    }
}