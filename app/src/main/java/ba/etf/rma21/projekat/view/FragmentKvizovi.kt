package ba.etf.rma21.projekat.view

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.AdapterView
import android.widget.ArrayAdapter
import android.widget.Spinner
import androidx.activity.OnBackPressedCallback
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.RecyclerView
import ba.etf.rma21.projekat.MainActivity
import ba.etf.rma21.projekat.R
import ba.etf.rma21.projekat.data.models.Kviz
import ba.etf.rma21.projekat.viewmodel.KvizRecyclerViewModel

class FragmentKvizovi : Fragment() {
    private lateinit var kvizovi: RecyclerView
    private lateinit var kvizoviAdapter: KvizoviAdapter
    private var kvizRecyclerViewModel= KvizRecyclerViewModel()
    private lateinit var spinner: Spinner

    fun onSuccess(kvizovi : List<Kviz>){
        kvizoviAdapter.updateKvizovi(kvizovi)
    }
    fun onError(){}

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        super.onCreate(savedInstanceState)
        setHasOptionsMenu(true)
        (activity as MainActivity).setBottomNav()
        var view = inflater.inflate(R.layout.kvizovi_fragment, container, false)
        kvizovi = view.findViewById(R.id.listaKvizova)
        kvizovi.layoutManager = GridLayoutManager(activity, 2)
        kvizoviAdapter=KvizoviAdapter(mutableListOf())
        kvizovi.adapter=kvizoviAdapter


        spinner=view.findViewById(R.id.filterKvizova)
        val opcije= arrayListOf("Svi moji kvizovi","Svi kvizovi","Urađeni kvizovi","Budući kvizovi","Prošli kvizovi")
        spinner.adapter= activity?.applicationContext?.let {
            ArrayAdapter<String>(
                it,
                android.R.layout.simple_spinner_dropdown_item,
                opcije
            )
        }


        spinner.onItemSelectedListener=object : AdapterView.OnItemSelectedListener{

            override fun onNothingSelected(parent: AdapterView<*>?) {
                kvizRecyclerViewModel.getUpisani(onSuccess = ::onSuccess, onError = ::onError)
            }

            override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
                if(position==0)
                    kvizRecyclerViewModel.getUpisani(onSuccess = ::onSuccess, onError = ::onError)
                else if(position==1)
                    kvizRecyclerViewModel.getAll(onSuccess = ::onSuccess, onError = ::onError)
                else if(position==2)
                kvizRecyclerViewModel.getDone(onSuccess = ::onSuccess, onError = ::onError)
                else if(position==3)
                    kvizRecyclerViewModel.getFuture(onSuccess = ::onSuccess, onError = ::onError)
                else if(position==4)
                    kvizRecyclerViewModel.getNotTaken(onSuccess = ::onSuccess, onError = ::onError)
            }
        }



        activity?.onBackPressedDispatcher?.addCallback(this, object : OnBackPressedCallback(true) {
            override fun handleOnBackPressed() {

            }
        })
        return view
    }

    companion object {
        fun newInstance(): FragmentKvizovi = FragmentKvizovi()
    }


  override fun onResume() {
      super.onResume()
      if(spinner.selectedItemPosition==0)
          kvizRecyclerViewModel.getUpisani(onSuccess = ::onSuccess, onError = ::onError)
      else if(spinner.selectedItemPosition==1)
          kvizRecyclerViewModel.getAll(onSuccess = ::onSuccess, onError = ::onError)
      else if(spinner.selectedItemPosition==2)
          kvizRecyclerViewModel.getDone(onSuccess = ::onSuccess, onError = ::onError)
      else if(spinner.selectedItemPosition==3)
          kvizRecyclerViewModel.getFuture(onSuccess = ::onSuccess, onError = ::onError)
      else if(spinner.selectedItemPosition==4)
          kvizRecyclerViewModel.getNotTaken(onSuccess = ::onSuccess, onError = ::onError)
  }
}

