package ba.etf.rma21.projekat.data.repositories

import ba.etf.rma21.projekat.data.models.ApiAdapter
import ba.etf.rma21.projekat.data.models.KvizTaken
import ba.etf.rma21.projekat.data.repositories.AccountRepository.Companion.acHash
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext

class TakeKvizRepository {

    companion object {

        suspend fun zapocniKviz(idKviza: Int): KvizTaken? {// kreira pokušaj za kviz, vraća kreirani pokušaj ili null u slučaju greške
            return withContext(Dispatchers.IO) {
                var zapoceti = getPocetiKvizovi()
                var pokusaj: KvizTaken = KvizTaken(0, "", null, 0, 0)
                //u listi zapocetih kvizova provjerimo ima li kviza sa zadatim id-em
                var nadjen = false
                if (zapoceti != null) {
                    for (kviz in zapoceti) {
                        if (kviz.id == idKviza) {
                            pokusaj = kviz
                            nadjen = true
                            break
                        }
                    }
                }
                val upisaniKvizovi = KvizRepository.getUpisani()
                var upisan = false
                for (kviz in upisaniKvizovi) {
                    if (kviz.id == idKviza) {
                        upisan = true
                        break
                    }
                }
                //ako nije pronadjen kviz, kreiraj pokusaj
                if (pokusaj.id == 0) {
                    pokusaj = ApiAdapter.retrofit.zapocniKviz(acHash, idKviza)
                } else if (upisan == false) return@withContext null
                //ako je pokusaj vec kreiran
                return@withContext pokusaj
            }
        }


        suspend fun getPocetiKvizovi(): List<KvizTaken>? {
            return withContext(Dispatchers.IO) {
                val zapoceti: List<KvizTaken> =
                    ApiAdapter.retrofit.getPocetiKvizovi(AccountRepository.getHash())

                if (zapoceti.size == 0) return@withContext null
                else return@withContext zapoceti
            }
        }
    }
}