package ba.etf.rma21.projekat.view

import android.os.Build
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.annotation.RequiresApi
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.RecyclerView
import ba.etf.rma21.projekat.MainActivity
import ba.etf.rma21.projekat.R
import ba.etf.rma21.projekat.data.models.Grupa
import ba.etf.rma21.projekat.data.models.Kviz
import ba.etf.rma21.projekat.data.models.Pitanje
import ba.etf.rma21.projekat.data.models.Predmet
import ba.etf.rma21.projekat.data.repositories.KvizRepository
import ba.etf.rma21.projekat.data.repositories.KvizRepository.Companion.getDone
import ba.etf.rma21.projekat.viewmodel.KvizRecyclerViewModel
import ba.etf.rma21.projekat.viewmodel.PitanjeKvizViewModel
import java.text.SimpleDateFormat
import java.util.*


class KvizoviAdapter(
private var kvizovi: List<Kviz>
) : RecyclerView.Adapter<KvizoviAdapter.KvizViewHolder>() {
private var model:KvizRecyclerViewModel= KvizRecyclerViewModel()

    val grupeZaKviz= mutableListOf<Grupa>()
    val upisaneGrupe= mutableListOf<Grupa>()
    val predmeti= mutableListOf<Predmet>()
    var done: List<Kviz> = arrayListOf<Kviz>()
    var future: List<Kviz> = arrayListOf<Kviz>()
    var notTaken: List<Kviz> = arrayListOf<Kviz>()
    var upisani: List<Kviz> = arrayListOf<Kviz>()
    var listaPitanja: List<Pitanje> = arrayListOf<Pitanje>()
    val modelpitanja=PitanjeKvizViewModel()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): KvizViewHolder {
        val view = LayoutInflater
            .from(parent.context)
            .inflate(R.layout.item_kviz, parent, false)
        return KvizViewHolder(view)
    }
    override fun getItemCount(): Int = kvizovi.size

    @RequiresApi(Build.VERSION_CODES.O)
    override fun onBindViewHolder(holder: KvizViewHolder, position: Int) {
        holder.naziv.text = kvizovi[position].naziv;
        model.getGrupeKvizDostupan(kvizovi[position].id,onSuccess = ::dajGrupe,onError = ::onError)
        model.getUpisaneGrupe(onSuccess = ::dajUpisane,onError = ::onError)
        //iz ovih grupa naci onu na koju je upisan
        holder.predmet.text = dajNazivPredmeta(dajIdPredmeta())
        holder.trajanje.text = kvizovi[position].trajanje.toString()+" min"
        if(kvizovi[position].osvojeniBodovi!=null) holder.osvojeno.text = kvizovi[position].osvojeniBodovi.toString();

        val trenutniDatum = Calendar.getInstance()
        val pattern = "dd.MM.yyyy"
        val simpleDateFormat = SimpleDateFormat(pattern)

        if(kvizovi[position].osvojeniBodovi!=null) {
            holder.status.setImageResource(R.drawable.plava)
            if(kvizovi[position].datumRada!=null) {
                val date = simpleDateFormat.format(kvizovi[position].datumRada)
                holder.datum.setText(date)
            }
                holder.osvojeno.setText(kvizovi[position].osvojeniBodovi.toString())
        }
        else if(kvizovi[position].osvojeniBodovi==null && trenutniDatum.getTime()>=kvizovi[position].datumPocetka &&
            ((kvizovi[position].datumKraj!=null && trenutniDatum.getTime()<=kvizovi[position].datumKraj) || kvizovi[position].datumKraj==null)){
            holder.status.setImageResource(R.drawable.zelena)
            if(kvizovi[position].datumKraj!=null){
                val date = simpleDateFormat.format(kvizovi[position].datumKraj)
                holder.datum.setText(date)
            }

            holder.osvojeno.setText("")
        }
        else if (kvizovi[position].osvojeniBodovi==null && trenutniDatum.getTime()<kvizovi[position].datumPocetka){
            holder.status.setImageResource(R.drawable.zuta)
            val date = simpleDateFormat.format(kvizovi[position].datumPocetka)
            holder.datum.setText(date)
            holder.osvojeno.setText("")
        }
        else if(kvizovi[position].osvojeniBodovi==null && trenutniDatum.getTime()>kvizovi[position].datumKraj){
            holder.status.setImageResource(R.drawable.crvena)
            val date = simpleDateFormat.format(kvizovi[position].datumKraj)
            holder.datum.setText(date)
            holder.osvojeno.setText("")
        }

        holder.itemView.setOnClickListener(object: View.OnClickListener{
            override fun onClick(v:View?) {
                val activity = v!!.context as AppCompatActivity

                model.getDone(onSuccess = ::Done, onError = ::onError)
                model.getNotTaken(onSuccess = ::notTaken, onError = ::onError)
                model.getFuture(onSuccess = ::future, onError = ::onError)
                model.getUpisani(onSuccess = ::upisani, onError = ::onError)

                if (done.contains(kvizovi[position]) || notTaken
                        .contains(kvizovi[position])
                    || future.contains(kvizovi[position]) || upisani
                        .contains(kvizovi[position])
                ) {
                    //plavi i crveni kvizovi (nasi)
                    if (done.contains(kvizovi[position]) || notTaken
                            .contains(kvizovi[position])
                    ) {
                        this@KvizoviAdapter.modelpitanja.getPitanja(kvizovi[position].id,onSuccess = ::pitanja, onError = ::onError)
                        val pokusaj = FragmentPokusaj(kvizovi[position], listaPitanja, "zatvoren")

                        activity.supportFragmentManager.beginTransaction()
                            .replace(R.id.container, pokusaj).addToBackStack(null).commit()
                    }
                    //zeleni kvizovi(nasi)
                    else if (!future.contains(kvizovi[position])) {
                        modelpitanja.getPitanja(kvizovi[position].id,onSuccess = ::pitanja, onError = ::onError)
                        val pokusaj = FragmentPokusaj(kvizovi[position],listaPitanja, "otvoren")

                        activity.supportFragmentManager.beginTransaction()
                            .replace(R.id.container, pokusaj).addToBackStack(null).commit()
                    }
                    //kvizovi koji nisu nasi i nasi zuti se ne mogu otvoriti
                }
            }
        })
    }

    fun updateKvizovi(kvizovi: List<Kviz>) {
        this.kvizovi = kvizovi
        notifyDataSetChanged()
    }
    fun dajGrupe(grupe:List<Grupa>){
        grupeZaKviz.clear()
        for(g in grupe){
            grupeZaKviz.add(g)
        }
    }
    fun dajUpisane(upisane:List<Grupa>){
        upisaneGrupe.clear()
        for(g in upisane){
            upisaneGrupe.add(g)
        }
    }

    fun dajIdPredmeta():Int{
        var predmetId=0
        for(i in grupeZaKviz){
            for(j in upisaneGrupe){
                if(i.id==j.id){
                    predmetId=i.idPredmeta
                }
            }
        }
        return predmetId
    }

    fun predmeti(p:List<Predmet>){
        for(i in p){
            predmeti.add(i)
        }
    }
    fun dajNazivPredmeta(id:Int):String{
        model.getPredmeti(onSuccess = ::predmeti,onError = ::onError)
        var naziv:String=""
        for(p in predmeti){
            if(p.id==id) naziv=p.naziv
        }
        return naziv
    }
    fun onError(){}
    fun Done(kvizovi: List<Kviz>) {
        done = kvizovi
    }

    fun future(kvizovi: List<Kviz>) {
        future = kvizovi
    }

    fun notTaken(kvizovi: List<Kviz>) {
        notTaken = kvizovi
    }

    fun upisani(kvizovi: List<Kviz>) {
        upisani = kvizovi
    }

    fun pitanja(pitanja:List<Pitanje>){
        listaPitanja=pitanja
    }
    inner class KvizViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val naziv: TextView = itemView.findViewById(R.id.naziv)
        val predmet: TextView = itemView.findViewById(R.id.predmet)
        val datum: TextView = itemView.findViewById(R.id.datum)
        val trajanje: TextView = itemView.findViewById(R.id.trajanje)
        val status: ImageView = itemView.findViewById(R.id.status)
        val osvojeno: TextView = itemView.findViewById(R.id.osvojeno)
    }
}
