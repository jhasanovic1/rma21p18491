package ba.etf.rma21.projekat.viewmodel

import ba.etf.rma21.projekat.data.models.*
import ba.etf.rma21.projekat.data.repositories.KvizRepository
import ba.etf.rma21.projekat.data.repositories.OdgovorRepository
import ba.etf.rma21.projekat.data.repositories.PredmetIGrupaRepository
import ba.etf.rma21.projekat.data.repositories.TakeKvizRepository
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.Job
import kotlinx.coroutines.launch


class KvizRecyclerViewModel {
    val scope = CoroutineScope(Job() + Dispatchers.Main)

    //fun getKvizes():List<Kviz>{
    //   return KvizRepository.getAll().sortedBy { it.datumPocetka }
    //}
    fun getAll(onSuccess: (kvizovi: List<Kviz>) -> Unit,
                    onError: () -> Unit) {
        scope.launch {
            val result =KvizRepository.getAll().sortedBy { it.datumPocetka }
            when (result) {
                is List<Kviz> -> onSuccess.invoke(result)
                else -> onError.invoke()
            }
        }
    }


    fun getDone(onSuccess: (kvizovi: List<Kviz>) -> Unit,
                onError: () -> Unit) {
        scope.launch {
            val result = KvizRepository.getDoneFilter().sortedBy { it.datumPocetka }
            when (result) {
                is List<Kviz> -> onSuccess.invoke(result)
                else -> onError.invoke()
            }
        }
    }
//    fun getDone(): List<Kviz> {
//        return KvizRepository.getDoneFilter().sortedBy { it.datumPocetka }
//    }

    fun getFuture(onSuccess: (kvizovi: List<Kviz>) -> Unit,
                onError: () -> Unit) {
        scope.launch {
            val result = KvizRepository.getFutureFilter().sortedBy { it.datumPocetka }
            when (result) {
                is List<Kviz> -> onSuccess.invoke(result)
                else -> onError.invoke()
            }
        }
    }
    fun getNotTaken(onSuccess: (kvizovi: List<Kviz>) -> Unit,
                onError: () -> Unit) {
        scope.launch {
            val result = KvizRepository.getNotTakenFilter().sortedBy { it.datumPocetka }
            when (result) {
                is List<Kviz> -> onSuccess.invoke(result)
                else -> onError.invoke()
            }
        }
    }

//    fun getFuture(): List<Kviz> {
//        return KvizRepository.getFutureFilter().sortedBy { it.datumPocetka }
//    }
//
//    fun getNotTaken(): List<Kviz> {
//        return KvizRepository.getNotTakenFilter().sortedBy { it.datumPocetka }
//    }

    fun getById(id:Int, onSuccess: (kviz: Kviz) -> Unit,
               onError: () -> Unit) {
        scope.launch {
            val result =KvizRepository.getById(id)
            when (result) {
                is Kviz -> onSuccess.invoke(result)
                else -> onError.invoke()//poruka da kviz ne postoji
            }
        }
    }

    fun getUpisani(onSuccess: (kvizovi: List<Kviz>) -> Unit,
                onError: () -> Unit) {
        scope.launch {
            val result =KvizRepository.getUpisani()
            when (result) {
                is List<Kviz> -> onSuccess.invoke(result)
                else -> onError.invoke()//poruka da kviz ne postoji
            }
        }
    }

    fun getGrupe(onSuccess: (grupe: List<Grupa>) -> Unit,
                 onError: () -> Unit) {
        scope.launch {
            val result =PredmetIGrupaRepository.getGrupe()
            when (result) {
                is List<Grupa> -> onSuccess.invoke(result)
                else -> onError.invoke()//poruka da kviz ne postoji
            }
        }
    }

    fun getGrupeZaPredmet(idPredmeta:Int, onSuccess: (grupe: List<Grupa>) -> Unit,
                 onError: () -> Unit) {
        scope.launch {
            val result =PredmetIGrupaRepository.getGrupeZaPredmet(idPredmeta)
            when (result) {
                is List<Grupa> -> onSuccess.invoke(result)
                else -> onError.invoke()
            }
        }
    }

    fun upisiUGrupu(idGrupa:Int, onSuccess: (uspjesno:Boolean) -> Unit,
                 onError: () -> Unit) {
        scope.launch {
            val result =PredmetIGrupaRepository.upisiUGrupu(idGrupa)
            when (result) {
                true -> onSuccess.invoke(result)
                false -> onError.invoke()//poruka da kviz ne postoji
            }
        }
    }

    fun getUpisaneGrupe(onSuccess: (grupe: List<Grupa>) -> Unit,
                 onError: () -> Unit) {
        scope.launch {
            val result =PredmetIGrupaRepository.getUpisaneGrupe()
            when (result) {
                is List<Grupa> -> onSuccess.invoke(result)
                else -> onError.invoke()//poruka da kviz ne postoji
            }
        }
    }

    fun zapocniKviz(idKviza:Int, onSuccess: (kvizTaken: KvizTaken) -> Unit,
                    onError: () -> Unit) {
        scope.launch {
            val result =TakeKvizRepository.zapocniKviz(idKviza)
            when (result) {
                is KvizTaken -> onSuccess.invoke(result)
                else -> onError.invoke()//poruka da kviz ne postoji
            }
        }
    }

    fun getPocetiKvizovi(onSuccess: (kvizoviTaken: List<KvizTaken>) -> Unit,
                    onError: () -> Unit) {
        scope.launch {
            val result =TakeKvizRepository.getPocetiKvizovi()
            when (result) {
                is List<KvizTaken> -> onSuccess.invoke(result)
                else -> onError.invoke()//poruka da kviz ne postoji
            }
        }
    }

    fun getOdgovoriKviz(idKviza:Int, onSuccess: (odgovori: List<Odgovor>) -> Unit,
                        onError: () -> Unit) {
        scope.launch {
            val result = OdgovorRepository.getOdgovoriKviz(idKviza)
            when (result) {
                is List<Odgovor> -> onSuccess.invoke(result)
                else -> onError.invoke()//poruka da kviz ne postoji
            }
        }
    }

    fun getPredmeti(onSuccess: (predmeti: List<Predmet>) -> Unit,
                    onError: () -> Unit) {
        scope.launch {
            val result = PredmetIGrupaRepository.getPredmeti()
            when (result) {
                is List<Predmet> -> onSuccess.invoke(result)
                else -> onError.invoke()
            }
        }
    }

    fun getPredmetId(id:Int,onSuccess: (predmet: Predmet?) -> Unit,
                     onError: () -> Unit) {
        scope.launch {
            val result = PredmetIGrupaRepository.getPredmetId(id)
            when (result) {
                is Predmet -> onSuccess.invoke(result)
                else -> onError.invoke() // poruka da predmet ne postoji
            }
        }
    }

    fun getNeupisaniPredmeti(onSuccess: (predmeti: List<Predmet>) -> Unit,
                             onError: () -> Unit) {
        scope.launch {
            val result = PredmetIGrupaRepository.getNeupisanePredmete()
            when (result) {
                is List<Predmet> -> onSuccess.invoke(result)
                else -> onError.invoke() // poruka da predmet ne postoji
            }
        }
    }

    fun getGrupeKvizDostupan(id:Int,onSuccess: (grupe: List<Grupa>) -> Unit,
                             onError: () -> Unit) {
        scope.launch {
            val result = PredmetIGrupaRepository.getGrupeKvizDostupan(id)
            when (result) {
                is List<Grupa> -> onSuccess.invoke(result)
                else -> onError.invoke()
            }
        }
    }

   /* fun postaviOdgovorKviz(idKvizTaken:Int, idPitanje:Int, odgovor:Int, onSuccess: (procenat: Int) -> Unit,
                    onError: () -> Unit) {
        scope.launch {
            val result =OdgovorRepository.postaviOdgovor(idKvizTaken,idPitanje,odgovor)
            when (result) {
                is Message -> onSuccess.invoke(result)
                else -> onError.invoke()//poruka da kviz ne postoji
            }
        }
    }*/
}