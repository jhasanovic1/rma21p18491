package ba.etf.rma21.projekat.data.repositories

import ba.etf.rma21.projekat.data.models.Account
import ba.etf.rma21.projekat.data.models.ApiAdapter
import ba.etf.rma21.projekat.data.models.Kviz
import ba.etf.rma21.projekat.data.models.Message
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext

class AccountRepository {
companion object {
    var acHash: String = "f432f56a-509c-46c9-874a-e6f697c2a507"

    fun postaviHash(acHash: String): Boolean {
        this.acHash = acHash
        return true
    }

    fun getHash(): String {
        return acHash
    }

    suspend fun getStudenta(hash:String): Account?{
        return withContext(Dispatchers.IO) {
            if(ApiAdapter.retrofit.getStudenta(hash)!!.id==0) return@withContext null
            else return@withContext ApiAdapter.retrofit.getStudenta(hash)
        }
    }

    suspend fun obrisiPodatkeZaStudenta():Message{
        return withContext(Dispatchers.IO) {
            return@withContext ApiAdapter.retrofit.obrisiPodatkeZaStudenta(AccountRepository.acHash)
        }
    }
}

}