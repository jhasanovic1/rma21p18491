
package ba.etf.rma21.projekat.view

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.activity.OnBackPressedCallback
import androidx.fragment.app.Fragment
import ba.etf.rma21.projekat.MainActivity
import ba.etf.rma21.projekat.R

class FragmentPoruka(val tekst:String,val a:String?,val b:Float?) : Fragment() {
    private lateinit var poruka: TextView

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        super.onCreate(savedInstanceState)
        setHasOptionsMenu(true)
        (activity as MainActivity).setBottomNav()
        var view = inflater.inflate(R.layout.poruka_fragment, container, false)
        poruka = view.findViewById(R.id.tvPoruka)
        var bundle=arguments
        val predmet= bundle?.getString("key")
        val grupa= bundle?.getString("key2")

        if(tekst.equals("upis")) {
            poruka.setText("Uspješno ste upisani u grupu " + grupa + " predmeta " + predmet + "!")
        }
        else if(tekst.equals("kviz")) {
            poruka.setText("Završili ste kviz " + a + " sa tačnosti " + (b!!*100).toString())
        }

        activity?.onBackPressedDispatcher?.addCallback(this, object : OnBackPressedCallback(true) {
            override fun handleOnBackPressed() {
               val kvizoviFragment = FragmentKvizovi.newInstance()
                fragmentManager?.beginTransaction()?.replace(R.id.container,kvizoviFragment)?.addToBackStack(null)?.commit()
                (activity as MainActivity).getBottomNav().selectedItemId=R.id.kvizovi
            }
        })
return view
    }

    companion object {
        fun newInstance(tekst: String,a:String?,b:Float?): FragmentPoruka= FragmentPoruka(tekst,a,b)
    }
}

