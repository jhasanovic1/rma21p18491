package ba.etf.rma21.projekat.data.repositories

import ba.etf.rma21.projekat.data.models.Pitanje

class PitanjeRepository {
    companion object {
        fun getOdgovore(pitanje: Pitanje):List<String>{
            return pitanje.opcije
        }
    }
}