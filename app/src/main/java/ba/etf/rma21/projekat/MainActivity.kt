package ba.etf.rma21.projekat

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import ba.etf.rma21.projekat.data.repositories.KvizRepository
import ba.etf.rma21.projekat.view.FragmentKvizovi
import ba.etf.rma21.projekat.view.FragmentPoruka
import ba.etf.rma21.projekat.view.FragmentPredmeti

import com.google.android.material.bottomnavigation.BottomNavigationView
import java.time.LocalDate
import java.util.*


class MainActivity : AppCompatActivity() {

    private lateinit var bottomNavigation: BottomNavigationView
    private lateinit var kviz:String
    private lateinit var predmet:String

    private val mOnNavigationItemSelectedListener =
        BottomNavigationView.OnNavigationItemSelectedListener { item ->
            when (item.itemId) {
                R.id.kvizovi -> {
                    val kvizoviFragment = FragmentKvizovi.newInstance()
                    openFragment(kvizoviFragment)
                    return@OnNavigationItemSelectedListener true
                }
                R.id.predmeti -> {
                    val predmetiFragment = FragmentPredmeti.newInstance()
                    openFragment(predmetiFragment)
                    return@OnNavigationItemSelectedListener true
                }
                /*R.id.predajKviz -> {
                    val bundle=Bundle()
                    bundle.putString("kviz",kviz)
                    bundle.putString("procenat",PitanjeKvizRepository.getProcenat(kviz,predmet).toString())
                    val porukaFragment = FragmentPoruka.newInstance("kviz",kviz,PitanjeKvizRepository.getProcenat(kviz,predmet))
                    porukaFragment.arguments= bundle
                    //za dati kviz postaviti bodove
                    var godina=0
                    var mjesec=0
                    var dan=0
                    val osvojeno=PitanjeKvizRepository.getProcenat(kviz,predmet)*KvizRepository.dajBodove(kviz,predmet)
                    godina=Calendar.getInstance().getTime().year
                    mjesec=Calendar.getInstance().getTime().month
                    dan=Calendar.getInstance().getTime().day
                    KvizRepository.azurirajKviz(kviz,predmet,osvojeno, Date(godina,mjesec,dan))
                    KvizRepository.predaj(kviz,predmet)
                    openFragment(porukaFragment)
                    return@OnNavigationItemSelectedListener true
                }*/
                R.id.zaustaviKviz -> {
                    //otvara kvizovi fragment
                    val kvizoviFragment = FragmentKvizovi.newInstance()
                    openFragment(kvizoviFragment)
                    return@OnNavigationItemSelectedListener true
                }

            }
            false
        }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        bottomNavigation = findViewById(R.id.bottomNav)
        bottomNavigation.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener)

//Defaultni fragment
        bottomNavigation.selectedItemId = R.id.kvizovi
        val kvizoviFragment = FragmentKvizovi.newInstance()
        openFragment(kvizoviFragment)
    }

    //Funkcija za izmjenu fragmenta
    fun openFragment(fragment: Fragment) {
        val transaction = supportFragmentManager.beginTransaction()
        transaction.replace(R.id.container, fragment)
        transaction.addToBackStack(null)
        transaction.commit()
    }

    fun setBottomNav() {
        bottomNavigation.menu.findItem(R.id.predajKviz).isVisible = false
        bottomNavigation.menu.findItem(R.id.zaustaviKviz).isVisible = false
        bottomNavigation.menu.findItem(R.id.kvizovi).isVisible = true
        bottomNavigation.menu.findItem(R.id.predmeti).isVisible = true
    }

    fun setBottomNav2() {
        bottomNavigation.menu.findItem(R.id.predajKviz).isVisible = true
        bottomNavigation.menu.findItem(R.id.zaustaviKviz).isVisible = true
        bottomNavigation.menu.findItem(R.id.kvizovi).isVisible = false
        bottomNavigation.menu.findItem(R.id.predmeti).isVisible = false
    }

    fun getBottomNav(): BottomNavigationView {
        return bottomNavigation
    }

    fun setVrijednosti(nazivKviza: String, nazivPredmeta: String){
        kviz=nazivKviza
        predmet=nazivPredmeta
    }

}
