/*

package ba.etf.rma21.projekat.view

import android.database.Cursor
import android.graphics.Color
import android.os.Bundle
import android.os.Handler
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.AdapterView
import android.widget.ArrayAdapter
import android.widget.ListView
import android.widget.TextView
import androidx.core.view.get
import androidx.fragment.app.Fragment
import ba.etf.rma21.projekat.R
import ba.etf.rma21.projekat.data.models.Kviz
import ba.etf.rma21.projekat.data.models.Pitanje
import ba.etf.rma21.projekat.data.repositories.KvizRepository
import ba.etf.rma21.projekat.data.repositories.PitanjeKvizRepository
import ba.etf.rma21.projekat.viewmodel.PitanjeViewModel


class FragmentPitanje(val kviz: Kviz, val pitanje:Pitanje) : Fragment() {
    private lateinit var tekstPitanja:TextView
    private lateinit var odgovori:ListView
    private var odabir=-1

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        super.onCreate(savedInstanceState)
        setHasOptionsMenu(true)

        odabir=PitanjeKvizRepository.getOdgovor(kviz,pitanje.naziv)

        var view=inflater.inflate(R.layout.fragment_pitanje, container, false)
        tekstPitanja = view.findViewById(R.id.tekstPitanja)
        tekstPitanja.setText(pitanje.tekstPitanja)
        odgovori= view.findViewById(R.id.odgovoriLista)
        val listaOdgovora=PitanjeViewModel().getPonudjeniOdgovori(pitanje)
        odgovori.adapter=activity?.applicationContext?.let { ArrayAdapter<String>(it,android.R.layout.simple_list_item_1,listaOdgovora) }

        if(odabir!=-1) {
            if(pitanje.tacan==odabir) odgovori.post{odgovori[odabir].setBackgroundColor(Color.parseColor("#3DDC84"))}
            else{
                odgovori.post{odgovori[odabir].setBackgroundColor(Color.parseColor("#DB4F3D"))}
                odgovori.post{odgovori[pitanje.tacan].setBackgroundColor(Color.parseColor("#3DDC84"))}
            }
            odgovori.isEnabled=false
        }
        if(odabir==-1 && kviz.predano==true){
            odgovori.post{odgovori[pitanje.tacan].setBackgroundColor(Color.parseColor("#3DDC84"))}
            odgovori.isEnabled=false
        }

        odgovori.onItemClickListener = object : AdapterView.OnItemClickListener {
            override fun onItemClick(parent: AdapterView<*>, view: View, position: Int, id: Long) {
                if(pitanje.tacan==position) {
                    parent.getChildAt(position).setBackgroundColor(Color.parseColor("#3DDC84"))
                    PitanjeKvizRepository.dodajOdgovor(kviz,pitanje.naziv,position)
                    KvizRepository.dodajTacan(kviz.naziv,kviz.nazivPredmeta)
                }
                else{
                    parent.getChildAt(position).setBackgroundColor(Color.parseColor("#DB4F3D"))
                    parent.getChildAt(pitanje.tacan).setBackgroundColor(Color.parseColor("#3DDC84"))
                    PitanjeKvizRepository.dodajOdgovor(kviz,pitanje.naziv,position)
                }
                odgovori.isEnabled=false

            }
        }

        return view
    }

    companion object {
        fun newInstance(kviz:Kviz,pitanje:Pitanje): FragmentPitanje = FragmentPitanje(kviz,pitanje)
    }
}


*/
