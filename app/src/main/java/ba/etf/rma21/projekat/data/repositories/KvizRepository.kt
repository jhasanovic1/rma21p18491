package ba.etf.rma21.projekat.data.repositories

import ba.etf.rma21.projekat.data.models.ApiAdapter
import ba.etf.rma21.projekat.data.models.Kviz
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import java.util.*
import kotlin.collections.ArrayList

class KvizRepository {
    companion object {
        init {
        }

    suspend fun getAll(): List<Kviz> {
        return withContext(Dispatchers.IO) {
            return@withContext ApiAdapter.retrofit.getAll()
        }
    }
    suspend fun getById(id:Int): Kviz? {//vraca kviz ili null ako kviz ne postoji
            return withContext(Dispatchers.IO) {
                if(ApiAdapter.retrofit.getById(id).id==0) return@withContext null
                else return@withContext ApiAdapter.retrofit.getById(id)
        }
    }
    suspend fun getKvizoviZaGrupu(idGrupe:Int): Array<Kviz>{
        return withContext(Dispatchers.IO) {
            return@withContext ApiAdapter.retrofit.getKvizoviZaGrupu(idGrupe)
        }
    }


    suspend fun getUpisani(): List<Kviz> {
        //dobijemo upisane grupe za studenta iz PredmetIGrupaRepository.getUpisaneGrupe()
        //prodjemo kroz listu i za svaku grupu pozovemo getKvizoviZaGrupu(idGrupe)

        return withContext(Dispatchers.IO) {
            val upisaneGrupe=PredmetIGrupaRepository.getUpisaneGrupe()
            var kvizoviZaGrupu:ArrayList<Kviz> = arrayListOf<Kviz>()

            for(grupa in upisaneGrupe){
                val lista= getKvizoviZaGrupu(grupa.id)
                for(l in lista){
                    kvizoviZaGrupu.add(l)
                }
            }
            return@withContext kvizoviZaGrupu
        }
   }

        suspend fun azurirajKviz(kviz: String, predmet: String, bodovi: Float, datum: Date){
            //nadjemo kviz u sviKvizovi i izmijenimo mu bodove i datum
            for(k in getAll()){
                if(k.naziv.equals(kviz) && k.nazivPredmeta.equals(predmet)){
                   k.osvojeniBodovi=bodovi
                    k.datumRada=Date(2007,2,28)
                }
            }
        }
        suspend fun dodajTacan(kviz:String,predmet: String){
            for(k in getAll()){
                if(k.naziv.equals(kviz) && k.nazivPredmeta.equals(predmet)){
                    k.tacnih++
                }
            }
        }

        suspend fun predaj(kviz:String,predmet: String){
            for(k in getAll()){
                if(k.naziv.equals(kviz) && k.nazivPredmeta.equals(predmet)){
                    k.predano=true
                }
            }
        }

        suspend fun dajBodove(kviz: String,predmet: String):Float?{
            var b:Float=0F
            for(k in getAll()){
                if(k.naziv.equals(kviz) && k.nazivPredmeta.equals(predmet)){
                    b= k.moguciBodovi!!
                }
            }
            return b
        }

        suspend fun getDoneFilter(): List<Kviz> {//svi urađeni moji
            return getUpisani().filter { it.osvojeniBodovi != null }
        }

        suspend fun getFutureFilter(): List<Kviz> {//moji budući
            val trenutniDatum = Calendar.getInstance().getTime()
            return getUpisani().filter { it.osvojeniBodovi == null && it.datumPocetka > trenutniDatum }
        }

        suspend fun getNotTakenFilter(): List<Kviz> {//moji neurađeni
            val trenutniDatum = Calendar.getInstance().getTime()
            var lista:ArrayList<Kviz> = arrayListOf<Kviz>()
            for(k in getUpisani()){
                if ((k.datumKraj != null && k.osvojeniBodovi == null && k.datumKraj < trenutniDatum) || k.datumKraj==null) {
                        lista.add(k)
                    }
            }
            return lista
        }

        suspend fun getDone(): List<Kviz> {//svi urađeni
            return getAll().filter { it.osvojeniBodovi != null }
        }

        suspend fun getFuture(): List<Kviz> {//svi budući
            val trenutniDatum = Calendar.getInstance().getTime()
            return  getAll().filter { it.osvojeniBodovi == null && it.datumPocetka > trenutniDatum }
        }

        suspend fun getNotTaken(): List<Kviz> {//svi neurađeni
            val trenutniDatum = Calendar.getInstance().getTime()
            var lista:ArrayList<Kviz> = arrayListOf<Kviz>()
            for(k in getAll()){
                if ((k.datumKraj != null && k.osvojeniBodovi == null && k.datumKraj < trenutniDatum) || k.datumKraj==null) {
                    lista.add(k)
                }
            }
            return lista
        }



    }
}

