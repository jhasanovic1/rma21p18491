

package ba.etf.rma21.projekat.view

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.activity.OnBackPressedCallback
import androidx.core.view.contains
import androidx.core.view.size
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentActivity
import ba.etf.rma21.projekat.MainActivity
import ba.etf.rma21.projekat.R
import ba.etf.rma21.projekat.data.models.Kviz
import ba.etf.rma21.projekat.data.models.Pitanje
import ba.etf.rma21.projekat.data.models.Predmet
import ba.etf.rma21.projekat.data.repositories.KvizRepository
import ba.etf.rma21.projekat.data.repositories.PitanjeKvizRepository
import com.google.android.material.navigation.NavigationView


class FragmentPokusaj(val kviz: Kviz, val listaPitanja: List<Pitanje>, val nav:String) : Fragment() {
    private lateinit var navigacija: NavigationView

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        super.onCreate(savedInstanceState)
        setHasOptionsMenu(true)
        if(nav.equals("otvoren")) (activity as MainActivity).setBottomNav2()
        else if(nav.equals("zatvoren")) (activity as MainActivity).setBottomNav()
        var view = inflater.inflate(R.layout.fragment_pokusaj, container, false)
        navigacija = view.findViewById(R.id.navigacijaPitanja)

        var i = 0
        for (pitanje in listaPitanja) {
            navigacija.menu.add(R.id.grupa, i, i, (i + 1).toString())
            i++
        }
        if(kviz.osvojeniBodovi!=null) navigacija.menu.add(R.id.grupa,i,i,"Rezultat");

        /*if(navigacija.menu.size>0) {
            val pitanje = FragmentPitanje(kviz,listaPitanja[0])
            activity?.supportFragmentManager?.beginTransaction()
                ?.replace(R.id.framePitanje, pitanje)?.addToBackStack(null)?.commit()
        }

        navigacija.setNavigationItemSelectedListener { menuItem ->
            when(menuItem.order) {
                navigacija.menu.size-1-> {
                    if(navigacija.menu.size==listaPitanja.size+1) {//ako ima pregled rezultata i ako se odabere pregled
                        (activity as FragmentActivity).supportFragmentManager.beginTransaction()
                            .replace(
                                R.id.framePitanje,
                                FragmentPoruka("kviz",kviz.naziv,PitanjeKvizRepository.getProcenat(kviz.naziv,kviz.nazivPredmeta))
                            ).addToBackStack(null).commit()
                    }
                    else{
                        (activity as FragmentActivity).supportFragmentManager.beginTransaction()
                            .replace(
                                R.id.framePitanje,
                                FragmentPitanje(kviz, listaPitanja[menuItem.order])
                            ).addToBackStack(null).commit()
                    }
                }
                else -> {//ako nema pregleda
                    (activity as FragmentActivity).supportFragmentManager.beginTransaction()
                        .replace(
                            R.id.framePitanje,
                            FragmentPitanje(kviz, listaPitanja[menuItem.order])
                        ).addToBackStack(null).commit()
                }
            }
            true;
        };*/


        activity?.onBackPressedDispatcher?.addCallback(this, object : OnBackPressedCallback(true) {
            override fun handleOnBackPressed() {
                val kvizoviFragment = FragmentKvizovi.newInstance()
                fragmentManager?.beginTransaction()?.replace(R.id.container,kvizoviFragment)?.addToBackStack(null)?.commit()
                (activity as MainActivity).getBottomNav().selectedItemId=R.id.kvizovi
            }
        })

//        (activity as MainActivity).setVrijednosti(kviz.naziv, kviz.nazivPredmeta)

        return view
    }

    companion object {
        fun newInstance(kviz: Kviz, predmet: Predmet, listaPitanja: List<Pitanje>,nav:String): FragmentPokusaj =
            FragmentPokusaj(kviz,listaPitanja,nav)
    }
}

