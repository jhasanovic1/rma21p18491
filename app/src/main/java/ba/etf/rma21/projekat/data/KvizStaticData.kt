/*
package ba.etf.rma21.projekat.data

import ba.etf.rma21.projekat.data.models.Kviz
import java.util.*

fun kvizovi(): List<Kviz> {
    return listOf(
        Kviz(1,"Kviz 1","UUP",
            Date(121,3,16),
            Date(122,3,17),null,
            5,"UTO 12:00", null,2F),
        Kviz(2,"Kviz 2","UUP",
            Date(122,2,16),
            Date(122,2,17),null,
            5,"PON 09:00", null,2F),
        Kviz(3,"Kviz 1","TP",
            Date(120,3,10),
            Date(120,3,17),Date(120,3,15),
            2,"PON 09:00", 3.1F,4F),
        Kviz(4,"Kviz 2","TP",
            Date(120,3,11),
            Date(120,3,17),Date(120,3,15),
            2,"UTO 12:00", 2.1F,3F),

        Kviz(5,"Kviz 2","RMA",
            Date(120,3,12),
            Date(120,3,17),Date(120,3,15),
            2,"PON 09:00", 1.6F,2F),
        Kviz(6,"Kviz 1","RMA",
            Date(120,3,13),
            Date(120,3,17),Date(120,1,1),
            2,"UTO 12:00", 0.75F,2F),
        Kviz(7,"Kviz 1","RPR",
            Date(121,3,14),
            Date(122,3,17),null,
            5,"PON 09:00", null,2F),
        Kviz(8,"Kviz 2","RPR",
            Date(122,3,15),
            Date(122,3,17),null,
            5,"UTO 12:00", null,2F),


        Kviz(9,"Kviz 1","WT",
            Date(122,3,17),
            Date(122,3,18),null,
            5,"UTO 12:00", null,2F),
        Kviz(10,"Kviz 2","WT",
            Date(122,1,16),
            Date(122,1,17),null,
            5,"PON 09:00", null,2F),
        Kviz(11,"Kviz 1","SI",
            Date(122,0,16),
            Date(122,3,17),null,
            5,"UTO 12:00", null,2F),
        Kviz(12,"Kviz 2","SI",
            Date(122,1,12),
            Date(122,1,17),null,
            5,"PON 09:00", null,2F),

        Kviz(13,"Kviz 2","MU",
            Date(120,2,10),
            Date(120,3,17),null,
            2,"PON 09:00", null,2F),
        Kviz(14,"Kviz 1","MU",
            Date(120,2,11),
            Date(120,3,17),Date(120,1,1),
            2,"UTO 12:00", 1.5F,2F),
        Kviz(15,"Kviz 1","RV",
            Date(121,3,6),
            Date(121,3,17),null,
            5,"PON 09:00", null,2F),
        Kviz(16,"Kviz 2","RV",
            Date(121,3,5),
            Date(121,3,17),null,
            5,"UTO 12:00", null,2F),

        Kviz(17,"Kviz 1","MPVI",
            Date(122,5,11),
            Date(122,5,17),null,
            5,"PON 09:00", null,2F),
        Kviz(18,"Kviz 2","MPVI",
            Date(121,2,13),
            Date(121,2,17),null,
            5,"UTO 12:00", null,2F),
        Kviz(19,"Kviz 1","SPO",
            Date(120,5,16),
            Date(120,5,17),Date(120,3,15),
            2,"PON 09:00", 2.0F,2F),
        Kviz(20,"Kviz 2","SPO",
            Date(120,3,17),
            Date(120,3,18),Date(120,3,15),
            2,"UTO 12:00", 0.5F,2F)
    )
}
*/
