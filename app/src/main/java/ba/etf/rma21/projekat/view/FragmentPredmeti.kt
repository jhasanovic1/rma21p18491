package ba.etf.rma21.projekat.view

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.AdapterView
import android.widget.ArrayAdapter
import android.widget.Button
import android.widget.Spinner
import androidx.activity.OnBackPressedCallback
import androidx.fragment.app.Fragment
import ba.etf.rma21.projekat.MainActivity
import ba.etf.rma21.projekat.R
import ba.etf.rma21.projekat.data.SpinnerData
import ba.etf.rma21.projekat.data.models.Grupa
import ba.etf.rma21.projekat.data.models.Predmet
import ba.etf.rma21.projekat.viewmodel.KvizRecyclerViewModel

class FragmentPredmeti : Fragment() {
    private lateinit var odabirGodina: Spinner
    private lateinit var odabirPredmet: Spinner
    private lateinit var odabirGrupa: Spinner
    private lateinit var dodajPredmetDugme: Button

    private var model = KvizRecyclerViewModel()

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View?{
        super.onCreate(savedInstanceState)
        setHasOptionsMenu(true)
        (activity as MainActivity).setBottomNav()
        var view=inflater.inflate(R.layout.upis_predmet_fragment, container, false)
        odabirGodina=view.findViewById(R.id.odabirGodina)
        odabirPredmet=view.findViewById(R.id.odabirPredmet)
        odabirGrupa=view.findViewById(R.id.odabirGrupa)
        dodajPredmetDugme=view.findViewById(R.id.dodajPredmetDugme)

        odabirGodina.setSelection(SpinnerData.getSpinner1())
        odabirPredmet.setSelection(SpinnerData.getSpinner2())
        odabirGrupa.setSelection(SpinnerData.getSpinner3())



        val godine=arrayOf("Odaberite godinu: ","1","2","3","4","5")
        odabirGodina.adapter= activity?.applicationContext?.let { ArrayAdapter<String>(it,android.R.layout.simple_spinner_dropdown_item,godine) }
        odabirPredmet.adapter= activity?.applicationContext?.let { ArrayAdapter<String>(it,android.R.layout.simple_spinner_dropdown_item,
            arrayOf("Odaberite predmet: ")) }
        odabirGrupa.adapter= activity?.applicationContext?.let { ArrayAdapter<String>(it,android.R.layout.simple_spinner_dropdown_item,arrayOf("Odaberite grupu: ")) }


        if(odabirGodina.selectedItemPosition==0){
            dodajPredmetDugme.isEnabled = false
            dodajPredmetDugme.isClickable = false
        }

        odabirGodina.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {//ODABIR GODINA
        override fun onNothingSelected(parent: AdapterView<*>?) {
            odabirGrupa.adapter = activity?.applicationContext?.let {
                ArrayAdapter<String>(
                    it, android.R.layout.simple_spinner_dropdown_item, mutableListOf("Odaberite grupu: "))}
            odabirPredmet.adapter = activity?.applicationContext?.let {
                ArrayAdapter<String>(
                    it, android.R.layout.simple_spinner_dropdown_item, mutableListOf("Odaberite predmet: "))}
            odabirGodina.adapter = activity?.applicationContext?.let {
                ArrayAdapter<String>(
                    it, android.R.layout.simple_spinner_dropdown_item, mutableListOf("Odaberite godinu: "))}
        }
            override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
                SpinnerData.setSpinner1(position)
                if(position==0){
                    odabirPredmet.setSelection(0)
                    odabirGrupa.setSelection(0)
                }
                if (position!=0) {
                    odabirPredmet.isEnabled = true
                    model.getNeupisaniPredmeti(onSuccess = ::onSuccess,onError = ::onError)
                }
            }
        }

        activity?.onBackPressedDispatcher?.addCallback(this, object : OnBackPressedCallback(true) {
            override fun handleOnBackPressed() {

                val kvizoviFragment = FragmentKvizovi.newInstance()
                fragmentManager?.beginTransaction()?.replace(R.id.container,kvizoviFragment)?.addToBackStack(null)?.commit()
                (activity as MainActivity).getBottomNav().selectedItemId=R.id.kvizovi
            }
        })

        return view
    }

    companion object{
        fun newInstance(): FragmentPredmeti = FragmentPredmeti()
    }

    fun onSuccess(neupisani: List<Predmet>){
        val predmetiNaGodini = neupisani.filter { predmet -> predmet.godina == odabirGodina.selectedItemId.toInt() }
        val naziviPredmeta = mutableListOf("Odaberite predmet: ")
        for(predmet in predmetiNaGodini)
            naziviPredmeta += predmet.naziv

        odabirPredmet.adapter = ArrayAdapter<String>(odabirGodina.context, android.R.layout.simple_spinner_dropdown_item, naziviPredmeta)

        odabirPredmet.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            /*override fun onNothingSelected(parent: AdapterView<*>?) {
                odabirPredmet.adapter= activity?.applicationContext?.let {
                    ArrayAdapter<String>(
                        it,android.R.layout.simple_spinner_dropdown_item,arrayOf("Odaberite grupu: "))
                }

            }*/
            override fun onNothingSelected(parent: AdapterView<*>?) {

            }

            override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
                SpinnerData.setSpinner2(position)
                if(position==0){
                    odabirGrupa.setSelection(0)
                    dodajPredmetDugme.isEnabled = false
                           dodajPredmetDugme.isClickable = false
                           odabirGrupa.adapter = activity?.applicationContext?.let {
                               ArrayAdapter<String>(
                                   it, android.R.layout.simple_spinner_dropdown_item, mutableListOf("Odaberite grupu: "))}
                }
                else {
                    odabirGrupa.isEnabled = true
                    model.getGrupeZaPredmet(predmetiNaGodini[position-1].id,onSuccess = ::onSuccess2,onError= ::onError)
                }
            }
        }
    }

    fun onSuccess2(neupisane : List<Grupa>){
        val naziviGrupa = mutableListOf("Odaberite grupu: ")
        for(grupa in neupisane)
            naziviGrupa += grupa.naziv

        odabirGrupa.adapter = ArrayAdapter<String>(odabirGodina.context, android.R.layout.simple_list_item_1, naziviGrupa)

        odabirGrupa.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onNothingSelected(parent: AdapterView<*>?) {
                odabirGrupa.adapter = activity?.applicationContext?.let {
                    ArrayAdapter<String>(
                        it, android.R.layout.simple_spinner_dropdown_item, mutableListOf("Odaberite grupu: "))}
            }

            override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
                SpinnerData.setSpinner3(position)
                if (position!=0) {
                    dodajPredmetDugme.isClickable = true
                    dodajPredmetDugme.isEnabled = true

                    dodajPredmetDugme.setOnClickListener {
                        SpinnerData.setSpinner1(odabirGodina.selectedItemPosition)
                        SpinnerData.setSpinner2(0)
                        SpinnerData.setSpinner3(0)

                       model.upisiUGrupu(neupisane[position-1].id,onSuccess = ::onSuccess3,onError = ::onError)
                    }

                }
            }
        }
    }

    fun onError(){}

    fun onSuccess3(uspjesno : Boolean){
        if(uspjesno==true) {
            val bundle = Bundle()
            bundle.putString("key", odabirPredmet.selectedItem.toString())
            bundle.putString("key2", odabirGrupa.selectedItem.toString())
            val porukaFragment = FragmentPoruka.newInstance("upis", null, null)
            porukaFragment.arguments = bundle
            fragmentManager?.beginTransaction()?.replace(R.id.container, porukaFragment)
                ?.addToBackStack(null)?.commit()
        }
        else return
    }
}
