package ba.etf.rma21.projekat.data.repositories

import ba.etf.rma21.projekat.data.models.ApiAdapter
import ba.etf.rma21.projekat.data.models.Message
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext

class GrupaRepository {
    companion object {
        init {
        }


        suspend fun upisiUGrupu(idGrupe: Int) : Message {
            return withContext(Dispatchers.IO) {
                return@withContext ApiAdapter.retrofit.upisiUGrupu(idGrupe, AccountRepository.getHash())
            }
        }

    }
}
