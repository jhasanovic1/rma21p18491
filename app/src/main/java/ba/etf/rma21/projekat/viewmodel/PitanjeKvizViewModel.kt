package ba.etf.rma21.projekat.viewmodel

import ba.etf.rma21.projekat.data.models.Grupa
import ba.etf.rma21.projekat.data.models.Pitanje
import ba.etf.rma21.projekat.data.repositories.PitanjeKvizRepository
import ba.etf.rma21.projekat.data.repositories.PredmetIGrupaRepository
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.Job
import kotlinx.coroutines.launch

class PitanjeKvizViewModel {
    val scope = CoroutineScope(Job() + Dispatchers.Main)

    /*fun getPitanjaZaKviz(kviz:String, predmet:String, onSuccess: (pitanja: List<Pitanje>) -> Unit,
                             onError: () -> Unit) {
        scope.launch {
            val result = PitanjeKvizRepository.getPitanja(kviz,predmet)
            when (result) {
                is List<Pitanje> -> onSuccess.invoke(result)
                else -> onError.invoke()
            }
        }
    }*/
    fun getPitanja(idKviza:Int, onSuccess: (grupe: List<Pitanje>) -> Unit,
                 onError: () -> Unit) {
        scope.launch {
            val result = PitanjeKvizRepository.getPitanja(idKviza)
            when (result) {
                is List<Pitanje> -> onSuccess.invoke(result)
                else -> onError.invoke()//poruka da kviz ne postoji
            }
        }
    }
}