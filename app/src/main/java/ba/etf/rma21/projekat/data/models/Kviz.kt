package ba.etf.rma21.projekat.data.models

import com.google.gson.annotations.SerializedName
import java.util.*

data class Kviz(
    @SerializedName("id") val id:Int,
    @SerializedName("naziv") val naziv: String,
    val nazivPredmeta: String,
    @SerializedName("datumPocetak") val datumPocetka: Date,
    @SerializedName("datumKraj") val datumKraj: Date?,
    var datumRada: Date?,
    @SerializedName("trajanje") val trajanje: Int,
    val nazivGrupe: String,
    var osvojeniBodovi: Float?,
    var moguciBodovi:Float,
    var tacnih: Int =0,
    var predano:Boolean=false
) {

}

